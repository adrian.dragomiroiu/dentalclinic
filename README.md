# DentalClinic

## Front-end
Acest proiect a fost generat cu [Angular CLI](https://github.com/angular/angular-cli) versiunea 16.1.0.

### Server de dezvoltare
Rulează `ng serve` pentru un server de dezvoltare. Navighează la `http://localhost:4200/`. Aplicația se va reîncărca automat dacă schimbi oricare dintre fișierele sursă.

### Generare de cod
Rulează `ng generate component component-name` pentru a genera un nou component. Poți de asemenea să folosești `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Compilare
Rulează `ng build` pentru a compila proiectul. Artefactele de compilare vor fi stocate în directorul `dist/`.

## Back-end
Acest proiect este un server Node.js construit folosind framework-ul Express.

Asigură-te că ai instalat Node.js și npm (Node Package Manager) pe sistemul tău. Poți descărca Node.js de la nodejs.org.

### Instalare
Pentru a instala dependențele necesare, rulează următoarea comandă în directorul proiectului:

```
npm install
```

### Pornirea serverului
Pentru a porni serverul, rulează următoarea comandă:

```
npm start
```

Serverul va rula pe http://localhost:8080/.